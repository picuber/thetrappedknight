from enum import Enum
import sys


class Board:
    class Dir(Enum):
        """
        direction indicator (up, down, left, right)
        """
        U, D, L, R = range(4)

    def __init__(self, radius):
        self.radius = radius
        self.size = 2*radius-1
        self.board = [[None for _ in range(self.size)]
                      for _ in
                      range(self.size)]
        x, y = self.radius-1, self.radius-1
        run = 1         # hom many fields to go straight for
        counter = 1     # counts the total number of that field
        direc = self.Dir.R

        self.board[x][y] = [counter, True, x, y]
        """
        :0: number of the field
        :1: wether it is unvisited (True = not yet visited,
                                    False = already visited)
        :2, 3: x, y position of this field
        """
        while not (direc == self.Dir.U and run == self.size):
            step = 0    # how many steps were completet in this run
            while step < run and counter < (self.size)**2:
                counter += 1
                x, y = self._next(x, y, direc)
                self.board[x][y] = [counter, True, x, y]
                step += 1
            direc, run = self._turn(direc, run)

    def _next(self, x, y, direc):
        """
        walk one step into the direction
        """
        if direc == self.Dir.U:
            return (x, y-1)
        elif direc == self.Dir.D:
            return (x, y+1)
        elif direc == self.Dir.L:
            return (x-1, y)
        else:
            return (x+1, y)

    def _turn(self, direc, run):
        """
        turn counter-clock wise
        increase the run after every second turn to build the spiral
        """
        if direc == self.Dir.U:
            return self.Dir.L, run+1
        elif direc == self.Dir.D:
            return self.Dir.R, run+1
        elif direc == self.Dir.L:
            return self.Dir.D, run
        else:
            return self.Dir.U, run

    def __repr__(self):
        out = ""
        for y in range(self.size):
            for x in range(self.size):
                out += str(self.board[x][y]) + ', '
            out = out[:-2] + '\n'
        return out

    def in_board(self, x, y):
        """
        checks if the position is inside the board
        """
        if 0 > x or x >= self.size:
            return False
        if 0 > y or y >= self.size:
            return False
        return True

    def fields(self, jumps):
        """
        get the fields for those positions in jump where it is inside the
        board and is not yet visited
        """
        return [self.board[x][y] for x, y in jumps
                if self.in_board(x, y) and self.board[x][y][1]]

    def visit(self, x, y):
        self.board[x][y][1] = False


def jumps(x, y, jump=(1, 2)):
    """
    generate the jumps in every direction for this class of jumps
    """
    j, k = jump
    return [(x+j, y+k), (x+j, y-k), (x-j, y+k), (x-j, y-k),
            (x+k, y+j), (x+k, y-j), (x-k, y+j), (x-k, y-j)]


def the_trapped_knight(radius=50, jump=(1, 2)):
    """run the trapped kight problem
    :size: (int) radius of the playing board (2*size-1 x 2*size-1)
    :jump: (int, int) how the knight can jump
    :fail: (bool) Throw an Error when the border is reached
    :returns: last board number the knight could go

    """
    board = Board(radius)
    x = board.radius-1
    y = board.radius-1
    path = []
    while True:
        board.visit(x, y)
        fields = board.fields(jumps(x, y, jump))
        if not fields:
            break
        num, _, x, y = min(fields)
        path.append(num)
    return path


def main():
    radius = 50
    jump = (1, 2)
    print_path = False

    if len(sys.argv) > 1:
        radius = int(sys.argv[1])

    if len(sys.argv) > 3:
        jump = int(sys.argv[2]), int(sys.argv[3])

    if len(sys.argv) > 4:
        print_path = True

    path = the_trapped_knight(radius, jump)

    if print_path:
        print(path)
    else:
        print(path[-1])


if __name__ == "__main__":
    main()
